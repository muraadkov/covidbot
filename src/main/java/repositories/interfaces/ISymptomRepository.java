package repositories.interfaces;

import domain.models.Symptom;

public interface ISymptomRepository extends IEntityRepository<Symptom> {
    Symptom getSymptomByID(long id);

    Symptom getSymptomByName(String name);
}
