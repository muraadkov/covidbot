package repositories;

import domain.models.Symptom;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.ISymptomRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class SymptomRepository implements ISymptomRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public void add(Symptom entity) {
        try {
            String sql = "INSERT INTO symptoms(name, definition) " +
                    "VALUES(?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getDefinition());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }


    @Override
    public void remove(Symptom entity) {

    }

    @Override
    public List<Symptom> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Symptom> symptoms = new LinkedList<>();
            while (rs.next()) {
                Symptom symptom = new Symptom(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("definition")
                );
                symptoms.add(symptom);
            }
            return symptoms;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public Symptom queryOne(String sql) { // ОРМ ????
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Symptom(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("definition")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    public Symptom getSymptomByID(long id) {
        String sql = "SELECT * FROM symptoms WHERE id = " + id + " LIMIT 1";
        return queryOne(sql);
    }


    public Symptom getSymptomByName(String name) {
        try {
            String sql = "SELECT * FROM symptoms WHERE name = ? LIMIT 1";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Symptom(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("definition")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }
}
