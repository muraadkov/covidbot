package domain.models;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class News {
    private Document document;


    public String getNewsKZ() {
        try {
            document = Jsoup.connect("https://www.zakon.kz/s/koronavirus-v-kazahstane/").get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elements = document.getElementsByClass("cat_news_item");
        String info = elements.text();
        int ind = 0, ind2 = 0;
        String kzNews = "";
        String kzNews2 = "";
        for (int i = 0; i < info.length(); i++) {
            if (info.charAt(i) == '2' && info.charAt(i + 1) == '0' && info.charAt(i + 2) == '2' && info.charAt(i + 3) == '0') {
                kzNews += info.substring(ind, i - 7) + "\n" + "\n";
                ind = i - 7;
            }

        }
        for(int i = 0 ; i < kzNews.length(); i++){
            if (kzNews.charAt(i) == ':') {
                kzNews2 += kzNews.substring(ind2, i - 2) + "\n";
                ind2 = i - 2;
            }
        }
        return kzNews2;
    }



    public String getTotalCases() {

        Elements elements = document.getElementsByClass("maincounter-number");
        return elements.text();

    }
}