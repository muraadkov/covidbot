package services;

import domain.models.Symptom;
import repositories.SymptomRepository;
import repositories.interfaces.ISymptomRepository;
import services.interfaces.ISymptomService;

public class SymptomService implements ISymptomService {
    private ISymptomRepository symptomRep = new SymptomRepository();

    public Symptom getSymptomByID(long id) {
        return symptomRep.getSymptomByID(id);
    }

    public Symptom getSymptomByName(String name) {
        return symptomRep.getSymptomByName(name);
    }

    public void addSymptom(Symptom symptom) {
        symptomRep.add(symptom);
    }

}
