package controllers;
import domain.models.Covid;
import domain.models.News;
import domain.models.Symptom;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import services.SymptomService;
import services.interfaces.ISymptomService;

import java.util.ArrayList;

public class BotController extends TelegramLongPollingBot {
    private final ISymptomService symptomService = new SymptomService();
    Covid covid = new Covid();
    News news = new News();
    private long chat_id;
    String lastMessage = "";
    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

    @Override
    public void onUpdateReceived(Update update) {
        update.getUpdateId();
        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId());
        chat_id = update.getMessage().getChatId();
        sendMessage.setText(getMessage(update.getMessage().getText()));
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        try{
            execute(sendMessage);
        } catch(TelegramApiException e){
            e.printStackTrace();
        }
    }



    public String getNews(){
        String info = news.getNewsKZ();
        return info;
    }


    public String getCovidInfo(String country){
        String info = covid.getCountry(country);
        return info;
    }

    public String getSymptom(String sympName){
        Symptom symptom = symptomService.getSymptomByName(sympName);
        if (symptom == null) return "Symptom with such name does not exist!";

        return symptom.toString();

    }

    public String getTop(){
        String info = covid.getTop();
        return info;
    }

    public String getMessage(String msg){

        ArrayList<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        KeyboardRow keyboardFourthRow = new KeyboardRow();
        KeyboardRow keyboardFifthRow = new KeyboardRow();
        KeyboardRow keyboardSixRow = new KeyboardRow();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        if(msg.equals("Меню") || msg.equals("/start")){
            keyboard.clear();
            keyboardFirstRow.clear();
            keyboardFirstRow.add("Статистика");
            keyboardFirstRow.add("Новости Казахстана");
            keyboardSecondRow.add("О болезни");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выбрать...";
        }

        if(msg.equals("О болезни")){
            keyboard.clear();
            keyboardFirstRow.clear();
            keyboardFirstRow.add("Информация");
            keyboardFirstRow.add("Симптомы");
            keyboardSecondRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выбрать...";
        }

        if(msg.equals("Статистика")){
            keyboard.clear();
            keyboardFirstRow.clear();
            keyboardFirstRow.add("Выбор стран");
            keyboardFirstRow.add("Топ стран по заболеванию");
            keyboardSecondRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выбрать...";
        }

        if(msg.equals("Новости Казахстана")){
            return getNews();
        }

        if(msg.equals("Топ стран по заболеванию")){
            return getTop();
        }

        if(msg.equals("Информация")){
            return "Коронавирусы вызывают заболевания млекопитающих (людей, летучих мышей, кошек, собак, свиней, крупного рогатого скота) и птиц. Источниками коронавирусных инфекций могут быть больной человек, животные. Возможные механизмы передачи: воздушно-капельный, воздушно-пылевой, фекально-оральный, контактный. Заболеваемость растёт зимой и ранней весной. В структуре ОРВИ госпитализированных больных коронавирусная инфекция составляет в среднем 12 %. Иммунитет после перенесённой болезни непродолжительный, как правило, не защищает от реинфекции. О широкой распространённости коронавирусов свидетельствуют специфичные антитела, выявленные у 80 % людей";
        }

        if(msg.equals("Выбор стран")){
            lastMessage = msg;
            keyboard.clear();
            keyboardFirstRow.add("UK");
            keyboardFirstRow.add("Kazakhstan");
            keyboardFirstRow.add("Russia");
            keyboardSecondRow.add("Brazil");
            keyboardSecondRow.add("China");
            keyboardSecondRow.add("Japan");
            keyboardThirdRow.add("Canada");
            keyboardThirdRow.add("France");
            keyboardThirdRow.add("India");
            keyboardFourthRow.add("Italy");
            keyboardFourthRow.add("Turkey");
            keyboardFourthRow.add("Germany");
            keyboardFifthRow.add("Mexico");
            keyboardFifthRow.add("Chile");
            keyboardFifthRow.add("Iran");
            keyboardSixRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);
            keyboard.add(keyboardFourthRow);
            keyboard.add(keyboardFifthRow);
            keyboard.add(keyboardSixRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выберите пункт меню";
        }



        if(msg.equals("Симптомы")){
            lastMessage = msg;
            keyboard.clear();
            keyboardFirstRow.add("Кашль");
            keyboardFirstRow.add("Утомляемость");
            keyboardFirstRow.add("Температура тела");
            keyboardSecondRow.add("Больно́е го́рло");
            keyboardSecondRow.add("Диарея");
            keyboardSecondRow.add("Конъюнктивит");
            keyboardThirdRow.add("Головная боль");
            keyboardThirdRow.add("Нарушение обоняния и чувства вкуса");
            keyboardFourthRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);
            keyboard.add(keyboardFourthRow);

            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выберите пункт меню";
        }

        if(lastMessage.equals("Выбор стран")){
            return getCovidInfo(msg.toLowerCase());
        } else if(lastMessage.equals("Симптомы")){
            return getSymptom(msg);
        }

        return "Напишите /start";
    }

    @Override
    public String getBotUsername() {
        return "@Covid19Bot";
    }

    @Override
    public String getBotToken() {
        return "1288060028:AAGpfYG2pP0JmmsQJ-Kgph0AdqPHeFOZBEY";
    }
}
